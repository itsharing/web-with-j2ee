<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
           <%-- <c:forEach var="post" items="${posts}">
           		<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${post.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						<div class="media-body">
    							<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${post.idUser }">${post.nameOfUser}</a></h5>
    							<small><i>${post.dateCreate}</i></small>
    							<p>${post.content }</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Thích</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Mỹ Linh và 11 người khác</a>
    							<div id="new-comment"></div>
								<c:forEach var="comment" items="${post.comments }">
									
    							<div class="media">
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${comment.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">${comment.nameOfUser }</a> ${comment.content }</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a></p>
    									<!-- <div class="media">
    										<a class="pull-left" href="#">
    											<img class="media-object" src="./assets/images/avatar/${comment.avatar}" alt="Image" width="50" height="auto">
    										</a>
    										<div class="media-body">
    											<h5 class="media-heading"><a href="#">Tên user</a> nội dung bình luận</h5>
    											<p><a href="#">Thích</a> . <a href="#">Trả lời</a>  . <a href=""><small>8 giờ</small></a></p>
    										</div>
    									</div> -->
    								</div>
    							</div>
    							<hr>
								</c:forEach>
									<img src="./assets/images/avatar/${account.profile.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" id="input-comment" class="my-input" placeholder="Bình luận" maxlength="300">
									<button type="button" id="comment" class="btn btn-primary">Bình luận</button>
									<input type="hidden" id="command-comment" name="command-comment" value="comment">
									<input type="hidden" id="id-post" value="${post.idPost }">
								
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>
           </c:forEach> --%>
           
           <div class="panel panel-default post">
              <div class="panel-body">
                 <div class="row">
                   <div class="col-sm-2">
                     <a href="profile.html" class="post-avatar thumbnail"><img src="<c:url value='/resources/images/avatar/${newPost.account.profile.avatar }'/>" alt=""><div class="text-center">${newPost.account.profile.name}</div></a>
                     <div class="likes text-center">0 Likes</div>
                   </div>
                   <div class="col-sm-10">
                     <div class="bubble">
                       <div class="pointer">
                         <p>${newPost.content }</p>
                       </div>
                       <div class="pointer-border"></div>
                     </div>
                     <c:set var="checkLike" value="false"></c:set>
                     <c:forEach var="likePost" items="${newPost.likePosts }">
                     	
                     	<c:if test="${likePost.post.id_post == newPost.id_post}">
                     		<p class="post-actions"><a href="#">Comment</a> - <a id="${newPost.id_post }-like" onclick="like('${newPost.id_post}')">UnLike</a> - <a href="#">Follow</a> - <a href="#">Share</a></p>
                     		<c:set var="checkLike" value="true"></c:set>
                     		<input type="hidden" value="liked"></input>
                     	</c:if>
                     	
                     	
                     		
                     </c:forEach>
                     <c:if test="${checkLike eq false}">
                     <p class="post-actions"><a href="#">Comment</a> - <a id="${newPost.id_post }-like" onclick="like('${newPost.id_post}')">Like</a> - <a href="#">Follow</a> - <a href="#">Share</a></p>
                     </c:if>
                     
                     
                     <div class="comment-form">
                       <form class="form-inline">
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="enter comment">
                        </div>
                        <button type="submit" class="btn btn-default">Add</button>
                      </form>
                     </div>
                     <div class="clearfix"></div>

                    <c:forEach var="comment" items="${newPost.comments }">
                    	<div class="comments">
                       <div class="comment">
                         <a href="#" class="comment-avatar pull-left"><img src="<c:url value='/resources/images/avatar/${comment.account.profile.avatar }'/>" alt=""></a>
                         <div class="comment-text">
                           <p>${comment.content }</p>
                         </div>
                       </div>
                       <div class="clearfix"></div>
                       
                     </div>
                    </c:forEach>
                   </div>
                 </div>
              </div>
            </div>
