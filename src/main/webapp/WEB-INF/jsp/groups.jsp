<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dobble Social Network: Group Page</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
  </head>

  <body>

  

    <%@include file="Header.jsp" %>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="groups">
              <h1 class="page-header">Groups</h1>
              <div class="group-item">
                <img src="img/group.png" alt="">
                <h4><a href="#">Sample Group One</a></h4>
                <p>Actually we will just write a quick paragraph. This is a sample Dobble social network group.</p>
                <p>
                  <a href="#" class="btn btn-default">Join Group</a>
                </p>
              </div>
              <div class="clearfix"></div>
              <div class="group-item">
                <img src="img/group.png" alt="">
                <h4><a href="#">Sample Group Two</a></h4>
                <p>Actually we will just write a quick paragraph. This is a sample Dobble social network group.</p>
                <p>
                  <a href="#" class="btn btn-default">Join Group</a>
                </p>
              </div>
              <div class="clearfix"></div>
              <div class="group-item">
                <img src="img/group.png" alt="">
                <h4><a href="#">Sample Group Three</a></h4>
                <p>Actually we will just write a quick paragraph. This is a sample Dobble social network group.</p>
                <p>
                  <a href="#" class="btn btn-default">Join Group</a>
                </p>
              </div>
              <div class="clearfix"></div>
              <div class="group-item">
                <img src="img/group.png" alt="">
                <h4><a href="#">Sample Group Four</a></h4>
                <p>Actually we will just write a quick paragraph. This is a sample Dobble social network group.</p>
                <p>
                  <a href="#" class="btn btn-default">Join Group</a>
                </p>
              </div>
            </div>
          </div>
          <%@include file="right_content.jsp" %>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <p>Dobble Copyright &copy, 2015</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>
