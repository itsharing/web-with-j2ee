
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dobble Social Network</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/css/bootstrap.css'/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
    <link href="<c:url value='/resources/css/font-awesome.css'/>" rel="stylesheet">
  
	
	
	
	
  </head>

  <body>

  

    <nav class="navbar navbar-default">
      <div class="container">
        
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="http://localhost:8080/web_hb_sp/">Home</a></li>
            <li><a href="viewMember">Members</a></li>
            <li><a href="contact.jsp">Contact</a></li>
            <li><a href="groups.jsp">Groups</a></li>
            <li><a href="viewPhoto">Photos</a></li>
            <li><a href="viewProfile">Profile</a></li>
             <li><a href="logOut">LogOut</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div><div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
    </nav>

    
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Wall</h3>
              </div>
              <div class="panel-body">
                
                  <div class="form-group">
                    <textarea id="input" class="form-control" placeholder="Write on the wall"></textarea>
                  </div>
                  <button id="post" type="button" class="btn btn-default" >Post</button>
                  <div class="pull-right">
                    
                  </div>
               
              </div>
            </div>
           
           <div id="new-post"></div>
           <c:forEach var="post" items="${account.posts }">
           		 <div class="panel panel-default post">
              <div class="panel-body">
                 <div class="row">
                   <div class="col-sm-2">
                     <a href="viewProfile?id_user=${post.account.id_user }" class="post-avatar thumbnail"><img src="<c:url value='/resources/images/avatar/${post.account.profile.avatar }'/>" alt=""><div class="text-center">${post.account.profile.name}</div></a>
                     
                     <div id="${post.id_post }-numberOfLike" class="likes text-center">${post.size} Likes</div>
                   </div>
                   <div class="col-sm-10">
                     <div class="bubble">
                       <div class="pointer">
                         <p>${post.content }</p>
                       </div>
                       <div class="pointer-border"></div>
                     </div>
                     <c:set var="checkLike" value="false"></c:set>
                     <c:forEach var="likePost" items="${post.likePosts }">
                     	
                     	<c:if test="${likePost.post.id_post == post.id_post and likePost.id_user == account.id_user}">
                     		<p class="post-actions"><a href="#">Comment</a> - <a id="${post.id_post }-like" onclick="like('${post.id_post}')">UnLike</a> - <a href="#">Follow</a> - <a href="#">Share</a></p>
                     		<c:set var="checkLike" value="true"></c:set>
                     		<input type="hidden" value="liked"></input>
                     	</c:if>
                     	
                     	
                     		
                     </c:forEach>
                     <c:if test="${checkLike eq false}">
                     <p class="post-actions"><a href="#">Comment</a> - <a id="${post.id_post }-like" onclick="like('${post.id_post}')">Like</a> - <a href="#">Follow</a> - <a href="#">Share</a></p>
                     </c:if>
                     
                     
                     <div class="comment-form">
                       <form class="form-inline">
                        <div class="form-group">
                          <input id="${post.id_post }-input-comment" type="text" class="form-control" placeholder="enter comment">
                        </div>
                        <button id="${post.id_post }-comment" onclick="newComment('${post.id_post}')" type="button" class="btn btn-default">Add</button>
                      </form>
                     </div>
                     <div class="clearfix"></div>
					
                    <c:forEach var="comment" items="${post.comments }">
                    	<div class="comments">
                       <div class="comment">
                         <a href="#" class="comment-avatar pull-left"><img src="<c:url value='/resources/images/avatar/${comment.account.profile.avatar }'/>" alt=""></a>
                         <div class="comment-text">
                           <p>${comment.content }</p>
                         </div>
                       </div>
                       <div class="clearfix"></div>
                       
                     </div>
                    </c:forEach>
                    	<div id="${post.id_post }-newComment"></div>
                   </div>
                 </div>
              </div>
            </div>
           
           </c:forEach>
            
          </div>
          <div class="col-md-4">
            <div class="panel panel-default friends">
              <div class="panel-heading">
                <h3 class="panel-title">My Friends</h3>
              </div>
              <div class="panel-body">
                <ul>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                  <li><a href="profile.html" class="thumbnail"><img src="img/user.png" alt=""></a></li>
                </ul>
                <div class="clearfix"></div>
                <a class="btn btn-primary" href="#">View All Friends</a>
              </div>
            </div>
            <div class="panel panel-default groups">
              <div class="panel-heading">
                <h3 class="panel-title">Latest Groups</h3>
              </div>
              <div class="panel-body">
                <div class="group-item">
                  <img src="img/group.png" alt="">
                  <h4><a href="#" class="">Sample Group One</a></h4>
                  <p>This is a paragraph of intro text, or whatever I want to call it.</p>
                </div>
                <div class="clearfix"></div>
                <div class="group-item">
                  <img src="img/group.png" alt="">
                  <h4><a href="#" class="">Sample Group Two</a></h4>
                  <p>This is a paragraph of intro text, or whatever I want to call it.</p>
                </div>
                <div class="clearfix"></div>
                <div class="group-item">
                  <img src="img/group.png" alt="">
                  <h4><a href="#" class="">Sample Group Three</a></h4>
                  <p>This is a paragraph of intro text, or whatever I want to call it.</p>
                </div>
                <div class="clearfix"></div>
                <a href="#" class="btn btn-primary">View All Groups</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    

    <footer>
      <div class="container">
        <p>Dobble Copyright &copy, 2015</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
    <script type="text/javascript">
		
	$(document).ready(function(){
	    $("#post").click(function(){
	    	if(!$.trim($("#input").val()))
        	{
        	    alert("Please enter some text in the post");
        	} else{
	    	var input = $("#input").val();
	    	
	    	
	        $.ajax({
	        	type: "POST",
	        	url: "post", 
	        	data:{input: input},
	        	success: function(result){
			    	$("#new-post").after(result.html);
			    	$("#input").val(null);
	        	}
	        	
	        });
	        }
	    });
	    
	   
	});
		    
		
	</script>
	
	 <script type="text/javascript">
    	function like(idPost){
    		
    			var obj = $("#"+idPost+"-like").html();
    	        if( obj != 'UnLike'){
    	        	obj = 'UnLike';
    	            $.ajax({
    	                 type: "GET",                     
    	                 url: "likePost", 
    	                 dataType: "text",
    	                 data:{idPost: idPost},
    	                 success: function(result){
    	                    $("#"+idPost+"-like").html(obj);
    	                    $("#"+idPost+"-numberOfLike").html(result+" Likes");
    	                    
    	                 }                     
    	             });   
    	            
    	        }
    	        else{   
    	        	//obj.data('liked', true);
    	        	obj = 'Like';
    	              $.ajax({
    	                 type: "GET",                     
    	                 url: "unlikePost", 
    	                 dataType: "text",
    	                 data:{idPost: idPost},
    	                 success: function(result){
    	                	 $("#"+idPost+"-like").html('Like');    
    	                	 $("#"+idPost+"-numberOfLike").html(result+" Likes");
    	                      }                     
    	                });  
    	            
    	        }
    	   
    	}

	</script> 
	
	<script type="text/javascript">
		function newComment(idPost){
			$(document).ready(function(){
				if(!$.trim($("#"+idPost+"-input-comment").val()))
	        	{
	        	    alert("Please enter some text in the comment");
	        	} else{
	        		var input = $("#"+idPost+"-input-comment").val();
			    	var command = $("#"+idPost+"-command-comment").val();
			        $.ajax({
			        	type: "POST",
			        	url: "comment", 
			        	data:{input: input, idPost:idPost},
			        	success: function(result){
					    	$("#"+idPost+"-newComment").before(result.html);
					    	$("#"+idPost+"-input-comment").val(null);
					    	
			        	}
			        	
			        });
	        	}
				    	
				   
				});
		}
	</script>
	
	
  </body>
</html>
