<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="row">
    				<div class="well">
    					<p><strong>About </strong><small><a href="#">. Edit</a></small></p>
    					<div class="row">
    						
    						<div class="col-xs-16 col-sm-16 col-md-16 col-lg-16">
    							<ul class="list-group">
    								<li class="list-group-item">Tên: <a href="#">${account.profile.name }</a></li>
    								<li class="list-group-item">Làm việc tại: <a href="#">${account.profile.job }</a></li>
    								<li class="list-group-item">Đang sống tại: <a href="#">${account.profile.city }</a></li>
    								<li class="list-group-item">SĐT: <a href="#">${account.profile.phone }</a></li>
    							</ul>
    						</div>
    					</div>
    				</div>
    			</div>