<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dobble Social Network: Members Page</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/css/bootstrap.css'/>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet">
    <link href="<c:url value='/resources/css/font-awesome.css'/>" rel="stylesheet">
  </head>

  <body>

  

    <%@include file="Header.jsp" %>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="members">
              <h1 class="page-header">Friendrs</h1>
              <c:forEach var="friend" items="${account.friends }">
	              	<div class="row member-row">
	                <div class="col-md-3">
	                  <img src="<c:url value='/resources/images/avatar/${friend.profile.avatar }'/>" class="img-thumbnail" alt="">
	                  <div class="text-center">
	                   	${friend.profile.name }
	                  </div>
	                </div>
	                <div class="col-md-3">
	                  <p><a href="#" class="btn btn-success btn-block">Delete Friend</a></p>
	                </div>
	                <div class="col-md-3">
	                  <p><a href="#" class="btn btn-primary btn-block">View Profile</a></p>
	                </div>
	              </div>
              </c:forEach>
              <!-- hien thi ban be -->
              
              
             
              
            </div>
          </div>
          <%@include file="right_content.jsp" %>
        </div>
      </div>
    </section>

    <footer>
      <div class="container">
        <p>Dobble Copyright &copy, 2015</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
  </body>
</html>
