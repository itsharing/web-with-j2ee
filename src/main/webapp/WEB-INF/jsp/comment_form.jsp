<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%-- <div class="comments">
                       <div class="comment">
                         <a href="#" class="comment-avatar pull-left"><img src="<c:url value='/resources/img/user.png'/>" alt=""></a>
                         <div class="comment-text">
                           <p>I am just going to paste in a paragraph, then we will add another clearfix.</p>
                         </div>
                       </div>
                       <div class="clearfix"></div>
                       
                     </div> --%>
                     <div class="comments">
                       <div class="comment">
                         <a href="#" class="comment-avatar pull-left"><img src="<c:url value='/resources/images/avatar/${newComment.account.profile.avatar }'/>" alt=""></a>
                         <div class="comment-text">
                           <p>${newComment.content }</p>
                          
                         </div>
                       </div>
                       <div class="clearfix"></div>
                       
                     </div>