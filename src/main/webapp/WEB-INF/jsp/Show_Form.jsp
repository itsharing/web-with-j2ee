<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="row">
    					<div class="panel panel-default">
    						<div class="panel-heading">
    							<div class="media">
    						<a class="pull-left" href="#">
    							<img class="media-object" src="./assets/images/avatar/${account.profile.avatar}" alt="Image" width="50" height="auto">
    						</a>
    						<div class="media-body">
    							<h5 class="media-heading"><a href="accountServlet?command=anotherUser&id=${account.idUser }">${account.profile.name}</a></h5>
    							<small><i>${article.dateCreate}</i></small>
    							<p>${article.content }</p>
    						</div>
    					</div>
    					<hr>
    					<ul class="list-inline">
    						<li><a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Thích</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-comment"></span> Bình Luận</a></li>
    						<li><a href="#"><span class="glyphicon glyphicon-link"></span> Chia Sẻ</a></li>
    					</ul>
    						</div>
    						<div class="panel-body">
    							<a href="#"><span class="glyphicon glyphicon-thumbs-up"></span> Mỹ Linh và 11 người khác</a>
    							<hr>
    							<div class="media">
    								<a class="pull-left" href="#">
    									<img class="media-object" width="50" height="auto" src="./assets/images/avatar/${account.profile.avatar}" alt="Image">
    								</a>
    								<div class="media-body">
    									<h5 class="media-heading"><a href="#">Xuan Cuong</a> nội dung bình luận</h5>
    									<p><a href="#">Thích</a> . <a href="#">Trả lời</a>  . <a href=""><small>8 giờ</small></a></p>
    									<div class="media">
    										<a class="pull-left" href="#">
    											<img class="media-object" src="./assets/images/avatar/${account.profile.avatar}" alt="Image" width="50" height="auto">
    										</a>
    										<div class="media-body">
    											<h5 class="media-heading"><a href="#">Tên user</a> nội dung bình luận</h5>
    											<p><a href="#">Thích</a> . <a href="#">Trả lời</a>  . <a href=""><small>8 giờ</small></a></p>
    										</div>
    									</div>
    								</div>
    							</div>
    							<hr>
								<form action="postServlet" method="POST">
									<img src="./assets/images/avatar/${account.profile.avatar}" class="img-thumbnail" width="45" alt="">
									<input type="text" class="my-input" placeholder="Bình luận">
									<button type="button" class="btn btn-primary">Bình luận</button>
								</form>
    							
    							
    						</div>
    					</div>

    					
    				
    			</div>