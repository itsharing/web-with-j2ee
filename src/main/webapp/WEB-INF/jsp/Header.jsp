<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="navbar-header" id="header_left">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="http://localhost:8080/web_hb_sp/"><span class="glyphicon glyphicon-info-sign"></span></a>
               </div>
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse navbar-ex1-collapse" id"header_right">
                 
                  <form class="navbar-form navbar-left" role="search" action="search" method="POST">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="data-search">
                     </div>
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     
                  </form>
                  <ul class="nav navbar-nav navbar-right"> 
                  	<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-user"></b></a>
                        <div class="dropdown-menu" id="my-size-dropdown">
	                        
                        </div>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-envelope"></b></a>
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-globe"></b></a>
                        
                     </li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<c:url value='/resources/images/avatar/${account.profile.avatar }'/>" alt="" width="20" height="20"><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                           <li><a href="changeProfile">Thiết lập tài khoản</a></li>
                           <li><a href="#">Thiết lập nội dung</a></li>
                           <li><a href="#">Thiết lập quyền riêng tư</a></li>
                           <li><a href="logout">Đăng xuât</a></li>
                        </ul>
                     </li>
                     
                  </ul>
               </div>
               <!-- /.navbar-collapse -->
            </div>
         </nav> --%>
         
         <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="">Home</a></li>
            <li><a href="viewFriend">Friends</a></li>
            <li><a href="viewPhoto">Photos</a></li>
            <li><a href="viewProfile">Profile</a></li>
            <li><a href="logout">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right" role="search" action="search" method="POST">
                     <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search" name="data-search">
                     </div>
                     <button type="submit" class="btn btn-default">search</button>
                     
           </form>
        </div><!--/.nav-collapse -->
      </div>
    </nav>