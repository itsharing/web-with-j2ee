package com.pnv.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Utils {
	public static String convertPasswordToMD5(String password) {

		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++)
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String normalizationDataSearch(String data_search) {
		data_search = data_search.trim();
		data_search = data_search.replaceAll("\\s+", " ");
        return data_search;
    }
	public static String convertDateCurrent(String dateInput) {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
		Date date;
		try {
			date = (Date)formatter.parse(dateInput);
			SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy");		
			return newFormat.format(date).toString();
		} catch (ParseException e) {
			return "01/01/2001";
		}	
	}
	
	public static Date convertStringToDate(String dateInput){
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = (Date) formatter .parse(dateInput);
			return date;
		} catch (Exception e) {
			return null;
		}
	}
	
//	public static void main(String[] args ){
//		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
//		try {
//			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//			Date date = (Date) formatter.parse("2017-03-02");
//			System.out.println(newFormat.parse(date.toString()));
//		} catch (Exception e) {
//			System.out.println("111");
//		}
//	}
	
	public static String randomCodeToVerifyEmail() {
		Random rnd = new Random();
		int MIN = 100000;
		int MAX = 1000000;
		int result = rnd.nextInt(MAX - MIN) + MIN;
		String code = Integer.toString(result);
		return code;
	}
	
	public static String verifyEmailToActivedAccount(String email, String name) {
		final String EMAIL = "it.sharing.website@gmail.com"; // hardCode
		final String PASSWORD = "administratordev17";
		String HOST = "mail.smtp.host";
		String COM = "smtp.gmail.com";
		String SOCKETPORT = "mail.smtp.socketFactory.port";
		String SOCKETCLASS = "mail.smtp.socketFactory.class";
		String SOCKETJAVAX = "javax.net.ssl.SSLSocketFactory";
		String AUTH = "mail.smtp.auth";
		String PORT = "mail.smtp.port";

		String TO = email;// change accordingly
		String CODE = Utils.randomCodeToVerifyEmail(); // TO return code

		// Get the session object
		Properties props = new Properties();
		props.put(HOST, COM);
		props.put(SOCKETPORT, "465");
		props.put(SOCKETCLASS, SOCKETJAVAX);
		props.put(AUTH, "true");
		props.put(PORT, "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(EMAIL, PASSWORD);// change
																	// accordingly
			}
		});
		// compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("EMAIL"));// change accordingly
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO));
			message.setSubject("VERIFY CODE");
			message.setText("Hello " + name + "\n" + "Please enter this code to finish register!" + "\n" + CODE);
			// send message
			Transport.send(message);
			// System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return CODE;
	}
	
}
