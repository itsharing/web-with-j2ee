package com.pnv.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.model.Account;
import com.pnv.model.Post;
import com.pnv.model.Relationship;

@Service
@Transactional
public class PostDaoImpl implements PostDao{
	@Autowired
	private SessionFactory sessionFactory;
	

	public List<Post> getAllPostOfUser(int id) {
		String strQuery = "from Post WHERE id_user=:id_user order by date_create DESC";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("id_user", id);
   
        List<Post> posts = query.list();
		return posts;
	}
	
	public void addPost(Post post) {
		sessionFactory.getCurrentSession().save(post);
		
	}


	public Post getAPost(int id_post) {
		Post post = (Post) sessionFactory.getCurrentSession().get(Post.class, id_post);
		return post;
	}

	public List<Relationship> getAllRelationshipOfUser(int id_user ) {
		String strQuery = "from Relationship WHERE id_user=:id_user";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("id_user", id_user);
        
        List<Relationship> relationshiops = query.list();
		return relationshiops;
	}



}
