package com.pnv.dao;

import java.util.List;

import com.pnv.model.*;

public interface CommentDao {
	List<Comment> getAllComment(int id_post);
	void addComment(Comment comment);
}
