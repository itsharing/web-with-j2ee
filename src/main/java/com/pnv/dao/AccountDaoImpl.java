package com.pnv.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.validation.groups.Default;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pnv.utils.*;
import com.pnv.model.Account;
import com.pnv.model.Profile;
import com.pnv.model.Relationship;



@Service
@Transactional
public class AccountDaoImpl implements AccountDao{
	@Autowired
	private SessionFactory sessionFactory;


	public Account checkIsActive(String email, String password) {
		String strQuery = "from Account WHERE email=:email AND password=:password";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("email", email);
        query.setParameter("password", Utils.convertPasswordToMD5(password));
   
        Account account = (Account)query.uniqueResult();
        return account;
	}


	public Account getAccount(String email, String password) {
		
        String strQuery = "from Account WHERE email=:email";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("email", email);
        Account account= (Account) query.uniqueResult();
		return account;
	}


	public Profile getProfile(int id) {
	     Profile profile = (Profile) sessionFactory.getCurrentSession().get(Profile.class, id);
	     return profile;
	}


	public Account getAccount(int id_user) {
		Account account = (Account) sessionFactory.getCurrentSession().get(Account.class, id_user);
	     return account;
	}
	
	public List<Profile> search(String name_user) {	
		
		String hql = "FROM Profile WHERE name LIKE :searchKey ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("searchKey", "%" + name_user + "%");
		List<Profile> results = new ArrayList<Profile>();
		results = query.list();		
		return results;
	}
	public List<Relationship> addFriend(Account account, int id_friend){	// add relationship	by session.save() Calendar.getInstance().getTime()
		Relationship relation = new Relationship( Calendar.getInstance().getTime(), null, account, id_friend, 0);
		sessionFactory.getCurrentSession().save(relation);
		List<Relationship> listRelation = new ArrayList<Relationship>() ;
		listRelation.add(relation);
		return listRelation;
	}

	public boolean agreeFriend(int id_user, int id_friend) {
		String hql = "update Relationship set status = 1 where id_user= :id_user and id_friend= :id_friend";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id_user", id_user);
		query.setParameter("id_friend", id_friend);
		int check = query.executeUpdate();		
		if(check == 1 ){
			return true;
		}
		return false;
	}

	public boolean deleteFriend(int id_user, int id_friend) {
		String hql = "update Relationship set status = 2 where id_user= :id_user and id_friend= :id_friend";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id_user", id_user);
		query.setParameter("id_friend", id_friend);
		int check = query.executeUpdate();	
		if(check == 1 ){
			return true;
		}
		return false;
	}

	public Account registerNewAccount(String email, String password) {
		Account account = new Account(email, password, 0, Utils.verifyEmailToActivedAccount(email, password), Calendar.getInstance().getTime());
		sessionFactory.getCurrentSession().save(account);
		return account;
	}

	public boolean registerNewProfile(Account account, String fullName, String dateOfBirth, int gender, String job,	String city) {
		
		Profile profile = new Profile(account,fullName, Utils.convertStringToDate(dateOfBirth), gender, null, job, city, null);
		sessionFactory.getCurrentSession().save(profile);
		return true;
	}


	public String getCode(String email, String password) {		
		String hql = "select verify_code from Account where email = :email and password = :password";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("email", email);
		query.setParameter("password", password);		
		return (String) query.uniqueResult();
	}

	public boolean isUpdateIsActive(Account account) {
		String hql = "UPDATE Account SET isActive = 1 WHERE email =:email";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("email", account.getEmail());
		int check = query.executeUpdate();
		if(check == 1 ){
			return true;
		}else 
			return false;
	}


	public Account getAccountById(int id) {
		String hql = "from Account where id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		Account account = (Account) query.uniqueResult();
		return account;
	}
	
	public List<Account> getAllAccount(){
		String hql = "from Account";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Account> accounts = query.list();
		return accounts;
	}


//	public List<Account> getAllFriend(int id) {
//		String hql = "from  where id = :id";
//		Query query = sessionFactory.getCurrentSession().createQuery(hql);
//		query.setParameter("id", id);
//		Account account = (Account) query.uniqueResult();
//		return account;
//		
//	}
	
}
