package com.pnv.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.model.Comment;

@Service
@Transactional
public class CommentDaoImpl implements CommentDao{
	@Autowired
	private SessionFactory sessionFactory;
	

	public List<Comment> getAllComment(int id_post) {
		String strQuery = "from Comment WHERE id_post=:id_post";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("id_post", id_post);
   
        List<Comment> comments = query.list();
		return comments;
	}


	public void addComment(Comment comment) {
		sessionFactory.getCurrentSession().save(comment);
	}

}
