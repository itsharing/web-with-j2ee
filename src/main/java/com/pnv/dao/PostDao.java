package com.pnv.dao;

import java.util.List;

import com.pnv.model.*;

public interface PostDao {
	List<Post> getAllPostOfUser(int id);
	void addPost(Post post);
	Post getAPost(int id_post);
	List<Relationship> getAllRelationshipOfUser(int id_user);
	//List<Post> getAllPostOfFriendUser(List<Relationship> listRelationship);
	
}
