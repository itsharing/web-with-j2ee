package com.pnv.dao;

import java.util.List;

import com.pnv.model.LikePost;
import com.pnv.model.Post;

public interface LikePostDao {
	void addALikeToPost(LikePost likePost);
	List<LikePost> selectNumberOfLike(int id_post);
	void deleteALikePost(LikePost likePost);
}
