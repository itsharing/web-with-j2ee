package com.pnv.dao;

import java.util.Date;
import java.util.List;


import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.stereotype.Repository;

import com.pnv.model.Account;
import com.pnv.model.Profile;
import com.pnv.model.Relationship;


public interface AccountDao{
	Account checkIsActive(String email, String password);
	Account getAccount(String email, String password);
	Account getAccount(int id_user);
	Profile getProfile(int id);
	List<Profile> search(String name_user);
	List<Relationship> addFriend(Account account, int id_friend);
	boolean agreeFriend(int id_user , int id_friend);
	boolean deleteFriend(int id_user , int id_friend);
	Account registerNewAccount(String email, String password);
	boolean registerNewProfile(Account account, String fullName, String dataOfbirth, int gender,String job, String city);
	String getCode(String email, String password);
	boolean isUpdateIsActive(Account account);
	Account getAccountById(int id);
	List<Account> getAllAccount();
//	List<Account> getAllFriend(int id);
}
