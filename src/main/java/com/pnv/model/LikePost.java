package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "like_post"
)
public class LikePost {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_like")
	private int id_like;
	
	@Column(name = "id_user")
	private int id_user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_post")
	@JsonIgnore
	private Post post;

	public LikePost() {
		super();
	}

	public LikePost(int id_like, int id_user) {
		super();
		this.id_like = id_like;
		this.id_user = id_user;
	}

	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public int getId_like() {
		return id_like;
	}

	public void setId_like(int id_like) {
		this.id_like = id_like;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	
	
	
}
