package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "like_comment"
)
public class LikeComment {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_like")
	private int id_like;
	
	@Column(name = "id_user")
	private int id_user;
	
	@Column(name = "id_comment")
	private int id_comment;

	
	
	public LikeComment() {
		super();
	}



	public LikeComment(int id_like, int id_user, int id_comment) {
		super();
		this.id_like = id_like;
		this.id_user = id_user;
		this.id_comment = id_comment;
	}



	public int getId_like() {
		return id_like;
	}



	public void setId_like(int id_like) {
		this.id_like = id_like;
	}



	public int getId_user() {
		return id_user;
	}



	public void setId_user(int id_user) {
		this.id_user = id_user;
	}



	public int getId_comment() {
		return id_comment;
	}



	public void setId_comment(int id_comment) {
		this.id_comment = id_comment;
	}
	
	
	
	
}
