package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "post"
)
public class Post {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_post")
	private int id_post;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "date_create")
	private Date date_create;
	
	@Column(name = "date_update")
	private Date date_update;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_user")
	@JsonIgnore
	private Account account;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "post")
	private List<Comment> comments;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "post")
	private List<LikePost> likePosts;
	
	

	public List<LikePost> getLikePosts() {
		return likePosts;
	}

	public void setLikePosts(List<LikePost> likePosts) {
		this.likePosts = likePosts;
	}

	public Post() {
		super();
	}

	public Post(int id_post, String content, Date date_create, Date date_update) {
		super();
		this.id_post = id_post;
		this.content = content;
		this.date_create = date_create;
		this.date_update = date_update;
	}
	

	public int getSize(){
		return likePosts.size();
	}


	public List<LikePost> getLikePosts() {
		return likePosts;
	}





	public void setLikePosts(List<LikePost> likePosts) {
		this.likePosts = likePosts;
	}





	public List<Comment> getComments() {
		return comments;
	}





	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}





	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public int getId_post() {
		return id_post;
	}

	public void setId_post(int id_post) {
		this.id_post = id_post;
	}

	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate_create() {
		return date_create;
	}

	public void setDate_create(Date date_create) {
		this.date_create = date_create;
	}

	public Date getDate_update() {
		return date_update;
	}

	public void setDate_update(Date date_update) {
		this.date_update = date_update;
	}

	
	
}
