package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;


import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pnv.model.Profile;



@Entity
@Table(name = "account"
)
@Transactional
public class Account implements java.io.Serializable{
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_user")
	private int id_user;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "isActive")
	private int isActive;
	
	@Column(name = "verify_code")
	private String verify_code;
	
	@Column(name = "date_create")
	private Date date_create;
	
	@Column(name = "date_update")
	private Date date_update;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "account")
    private Profile profile;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "account")
	private List<Post> posts;	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "account")
	private List<Comment> comments;
	
	@Transient
	private List<Account> friends;
	
	
	
	
	
	
	
	
	
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "account_friend")
//	private List<Relationship> relationShip_friend;

	public Account() {
		
	}	
	
	
	
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	
	public List<Account> getFriends() {
		return friends;
	}



	public void setFriends(List<Account> friends) {
		this.friends = friends;
	}



	public Account(int id_user, String email, String password, int isActive, String verify_code, Date date_create,
			Date date_update) {
		
		this.id_user = id_user;
		this.email = email;
		this.password = password;
		this.isActive = isActive;
		this.verify_code = verify_code;
		this.date_create = date_create;
		this.date_update = date_update;
	}


<<<<<<< HEAD
	public Account(String email, String password, int isActive, String verify_code, Date date_create) {
		super();
		this.email = email;
		this.password = password;
		this.isActive = isActive;
		this.verify_code = verify_code;
		this.date_create = date_create;
	}
	
=======

>>>>>>> c1968e8077350e58572617753ac807a6616677c8
	public List<Post> getPosts() {
		return posts;
	}



	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}



	public Profile getProfile() {
		return profile;
	}



	public void setProfile(Profile profile) {
		this.profile = profile;
	}



	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getVerify_code() {
		return verify_code;
	}

	public void setVerify_code(String verify_code) {
		this.verify_code = verify_code;
	}



	public Date getDate_create() {
		return date_create;
	}



	public void setDate_create(Date date_create) {
		this.date_create = date_create;
	}



	public Date getDate_update() {
		return date_update;
	}



	public void setDate_update(Date date_update) {
		this.date_update = date_update;
	}
	
	
	
	
	
}
