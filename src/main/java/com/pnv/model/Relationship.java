package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
@Entity
@Table(name = "relationship"
)
public class Relationship {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	private int id;	
		
	@Column(name = "status")
	private int status;
	
	@Column(name = "date_create")
	private Date date_create;
	
	@Column(name = "date_update")
	private Date date_update;
	
	@Column(name = "id_user1")
	private int id_user1;
	
	@Column(name = "id_user2")
	private int id_user2;

	
	public Relationship() {
		super();
	}

	public Relationship(int id, int status, Date date_create, Date date_update) {
		super();
		this.id = id;		
		this.status = status;
		this.date_create = date_create;
		this.date_update = date_update;
	}	

	public Relationship(Date date_create, Date date_update, Account account,int id_friend, int status) {
		super();		
		this.date_create = date_create;
		this.date_update = date_update;
		this.status= status;
	}
	
	

	
	public int getId_user1() {
		return id_user1;
	}

	public void setId_user1(int id_user1) {
		this.id_user1 = id_user1;
	}

	public int getId_user2() {
		return id_user2;
	}

	public void setId_user2(int id_user2) {
		this.id_user2 = id_user2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public Date getDate_create() {
		return date_create;
	}


	public void setDate_create(Date date_create) {
		this.date_create = date_create;
	}


	public Date getDate_update() {
		return date_update;
	}


	public void setDate_update(Date date_update) {
		this.date_update = date_update;
	}
	
	
	
	
}
