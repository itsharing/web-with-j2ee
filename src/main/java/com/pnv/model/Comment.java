package com.pnv.model;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "comment"
)
public class Comment {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_comment")
	private int id_comment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_user")
	@JsonIgnore
	private Account account;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "date_create")
	private Date date_create;
	
	@Column(name = "id_parent")
	private int id_parent;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_post")
	@JsonIgnore
	private Post post;

	public Comment() {
		super();
	}

	public Comment(int id_comment, String content, Date date_create, int id_parent) {
		super();
		this.id_comment = id_comment;
		this.content = content;
		this.date_create = date_create;
		this.id_parent = id_parent;
	}
	

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public int getId_comment() {
		return id_comment;
	}

	public void setId_comment(int id_comment) {
		this.id_comment = id_comment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate_create() {
		return date_create;
	}

	public void setDate_create(Date date_create) {
		this.date_create = date_create;
	}

	public int getId_parent() {
		return id_parent;
	}

	public void setId_parent(int id_parent) {
		this.id_parent = id_parent;
	}
	
	
	
	
	
}
