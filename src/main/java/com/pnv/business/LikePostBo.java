package com.pnv.business;

import java.util.List;

import com.pnv.model.*;

public interface LikePostBo {
	List<Post> getAllLikePost(List<Post> posts);
	List<LikePost> like(LikePost likePost);
	List<LikePost> unlikePost(LikePost likePost);
}
