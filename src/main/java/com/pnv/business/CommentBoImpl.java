package com.pnv.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.CommentDao;
import com.pnv.model.Comment;
import com.pnv.model.Post;
@Service
@Transactional
public class CommentBoImpl implements CommentBo{
	
	@Autowired
	private CommentDao commentDao;
	

	public List<Post> getAllComment(List<Post> posts) {
		for(int i = 0; i < posts.size(); i++){
			posts.get(i).setComments(commentDao.getAllComment(posts.get(i).getId_post()));
		}
		return posts;
	}


	public List<Comment> comment(Comment comment) {
		commentDao.addComment(comment);
		List<Comment> comments = commentDao.getAllComment(comment.getPost().getId_post());
		return comments;
	}

	

}
