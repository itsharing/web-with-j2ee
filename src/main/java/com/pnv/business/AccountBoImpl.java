package com.pnv.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.AccountDao;
import com.pnv.dao.CommentDao;
import com.pnv.dao.PostDao;
import com.pnv.model.Account;
import com.pnv.model.Comment;
import com.pnv.model.Post;
import com.pnv.model.Profile;

@Service
@Transactional
public class AccountBoImpl implements AccountBo{

	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private PostDao postDao;
	
	@Autowired
	private CommentBo commentBo;
	
	@Autowired
	private PostBo postBo;
	
	
	
	public Account login(String email, String password) {
		if(accountDao.checkIsActive(email, password) != null){
			Account account = accountDao.getAccount(email, password);
			Profile profile = accountDao.getProfile(account.getId_user());
			List<Post> posts = postBo.getAllPost(account.getId_user());
			for (Post post : posts) {
				post.getAccount().setProfile(accountDao.getProfile(post.getAccount().getId_user()));
			}
			account.setProfile(profile);
			account.setPosts(posts);
			return account;
		}else{
			return null;
		}
		
	}
	
}
