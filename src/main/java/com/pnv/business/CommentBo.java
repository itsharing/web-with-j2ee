package com.pnv.business;

import java.util.List;

import com.pnv.model.Comment;
import com.pnv.model.Post;

public interface CommentBo {
	List<Post> getAllComment(List<Post> posts);
	List<Comment> comment(Comment comment);
}
