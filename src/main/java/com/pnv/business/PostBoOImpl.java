package com.pnv.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.PostDao;
import com.pnv.model.Post;
import com.pnv.model.*;


@Service
@Transactional
public class PostBoOImpl implements PostBo{

	@Autowired
	private PostDao postDao;
	
	@Autowired
	private LikePostBo likePostBo;
	
	@Autowired
	private CommentBo commentBo;
	

	public List<Post> getAllPost(int id_user) {
		List<Post> allPost = new ArrayList<Post>();
		
		List<Post> posts = postDao.getAllPostOfUser(id_user);
		allPost.addAll(posts);
		
		List<Relationship> listRelationship = postDao.getAllRelationshipOfUser(id_user);
		
		for (Relationship relationship : listRelationship) {
			List<Post> listPost = postDao.getAllPostOfUser(relationship.getId_friend());
			allPost.addAll(listPost);
		}
		
		allPost = likePostBo.getAllLikePost(allPost);
		allPost = commentBo.getAllComment(allPost);
		
		return allPost;
	}


	public List<Post> post(Post post) {
		postDao.addPost(post);
		List<Post> posts = postDao.getAllPostOfUser(post.getAccount().getId_user());
		return posts;
	}
	
}
