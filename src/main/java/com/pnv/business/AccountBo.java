package com.pnv.business;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.pnv.model.Account;
import com.pnv.model.Profile;
import com.pnv.model.Relationship;

public interface AccountBo {
	Account login(String email, String password);
	List<Profile> search(String name);
	List<Relationship> addFriend(Account account, int id_friend);
	boolean agreeFriend(int id_user, int id_friend);
	boolean deleteFriend(int id_user, int id_friend);
	Account addNewAccount(String email, String password);
	boolean addNewProfile(Account account, String fullName, int gender, String dateOfBirth, String job, String city);
	String getVerifyCode(String email, String password);
	boolean updateIsActive(Account account);
	Account getAccountById(int id);
	List<Account> getAllFriend(int id);
}
