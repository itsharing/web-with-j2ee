package com.pnv.business;

import java.util.List;

import com.pnv.model.*;

public interface PostBo {
	List<Post> getAllPost(int id_user);
	List<Post> post(Post post);
}
