package com.pnv.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.AccountBo;
import com.pnv.business.LikePostBo;
import com.pnv.model.Account;
import com.pnv.model.LikePost;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LikeController {
	
	@Autowired
    private LikePostBo likePostBo;
 
 @RequestMapping(value = "/likePost", method = RequestMethod.GET)
    public void likePost(HttpServletRequest request ,HttpServletResponse response, @RequestParam("idPost") int idPost) {
  Account account = (Account) request.getSession().getAttribute("account");
  LikePost likePost = new LikePost();
  List<LikePost> likePosts = null;
  likePost.setId_user(account.getId_user());
  for(int i= 0; i < account.getPosts().size();i++){
   if(account.getPosts().get(i).getId_post() == idPost){
    likePost.setPost(account.getPosts().get(i));
    likePosts =  likePostBo.like(likePost);
    account.getPosts().get(i).setLikePosts(likePosts);
    break;
   }
   
  }
  
        request.getSession().removeAttribute("account");
        request.getSession().setAttribute("account", account);
        response.setContentType("text");
        try {
   response.getWriter().write(""+likePosts.size());
  } catch (IOException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
    }
 
 @RequestMapping(value = "/unlikePost", method = RequestMethod.GET)
    public void unlikePost(HttpServletRequest request ,HttpServletResponse response, @RequestParam("idPost") int idPost) {
  Account account = (Account) request.getSession().getAttribute("account");
  LikePost likePost = new LikePost();
  List<LikePost> likePosts = null;
  likePost.setId_user(account.getId_user());
  for(int i= 0; i < account.getPosts().size();i++){
   if(account.getPosts().get(i).getId_post() == idPost){
    likePost.setPost(account.getPosts().get(i));
    likePosts =  likePostBo.unlikePost(likePost);
    account.getPosts().get(i).setLikePosts(likePosts);
    break;
   }
   
  }
  
        request.getSession().removeAttribute("account");
        request.getSession().setAttribute("account", account);
        response.setContentType("text");
        try {
   response.getWriter().write(""+likePosts.size());
  } catch (IOException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
    }
}
