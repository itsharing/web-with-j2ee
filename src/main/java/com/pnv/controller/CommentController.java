package com.pnv.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.pnv.business.CommentBo;
import com.pnv.dao.PostDao;
import com.pnv.model.Account;
import com.pnv.model.Comment;
import com.pnv.model.Post;

@Controller
public class CommentController {
	@Autowired
	 private CommentBo commentBo;
	 
	 @Autowired
	 private PostDao postDao;
	 
	 @RequestMapping(value = "/comment", method = RequestMethod.POST)
	    public void post(HttpServletRequest request, HttpServletResponse response, @RequestParam("input") String content,@RequestParam("idPost") int id_post) {
	  Account account = (Account) request.getSession().getAttribute("account");
	  Comment comment = new Comment();
	  Date date = new Date();
	  date.getTime();
	  Post post = postDao.getAPost(id_post);
	  post.setAccount(account);
	  comment.setDate_create(date);
	  comment.setContent(content);
	  comment.setId_parent(id_post);
	  comment.setAccount(account);
	  comment.setPost(post);
	  
	  for(int i = 0; i < account.getPosts().size(); i++){
	   if(account.getPosts().get(i).getId_post() == id_post){
	    account.getPosts().get(i).setComments(commentBo.comment(comment));
	   }
	  }
	  request.getSession().removeAttribute("account");
	  request.getSession().setAttribute("account", account);
	  request.setAttribute("newComment", comment);
	  final StringWriter buffer = new StringWriter();
	        try {
	   request.getRequestDispatcher("WEB-INF/jsp/comment_form.jsp").include(request, new HttpServletResponseWrapper(response) {
	       private PrintWriter writer = new PrintWriter(buffer);

	       @Override
	       public PrintWriter getWriter() throws IOException {
	           return writer;
	       }
	   });
	  } catch (ServletException e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  } catch (IOException e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }

	        String html = buffer.toString();
	        Map<String, Object> data = new HashMap<String, Object>();
	        data.put("html", html);
	  response.setContentType("application/json");  
	  response.setCharacterEncoding("UTF-8"); 
	  try {
	   response.getWriter().write(new Gson().toJson(data));
	  } catch (IOException e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }  
	  
	    }
}
