package com.pnv.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.pnv.business.PostBo;
import com.pnv.model.Account;
import com.pnv.model.Post;

@Controller
public class PostController {
	@Autowired
	private PostBo postBo;
	
	@RequestMapping(value = "/post", method = RequestMethod.POST)
    public void post(HttpServletRequest request, HttpServletResponse response, @RequestParam("input") String content) {
		Account account = (Account) request.getSession().getAttribute("account");
		Post post = new Post();
		Date date = new Date();
		date.getTime();
		post.setDate_create(date);
		post.setContent(content);
		post.setAccount(account);
		account.setPosts(postBo.post(post));
		request.getSession().removeAttribute("account");
		request.getSession().setAttribute("account", account);
		request.setAttribute("newPost", post);
		final StringWriter buffer = new StringWriter();
        try {
			request.getRequestDispatcher("WEB-INF/jsp/post_form.jsp").include(request, new HttpServletResponseWrapper(response) {
			    private PrintWriter writer = new PrintWriter(buffer);

			    @Override
			    public PrintWriter getWriter() throws IOException {
			        return writer;
			    }
			});
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        String html = buffer.toString();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("html", html);
		response.setContentType("application/json");  
		response.setCharacterEncoding("UTF-8"); 
		try {
			response.getWriter().write(new Gson().toJson(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
    }
}
