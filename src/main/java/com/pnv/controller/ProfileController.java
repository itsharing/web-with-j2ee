package com.pnv.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.pnv.business.PostBo;
import com.pnv.dao.AccountDao;
import com.pnv.model.Account;
import com.pnv.model.Post;
import com.pnv.model.Profile;

@Controller
public class ProfileController {
	
	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private PostBo postBo;
	
	@RequestMapping(value = "/viewProfile", method = RequestMethod.GET)
    public String viewProfile(HttpServletRequest request,HttpServletResponse response, @RequestParam("id_user") int id_user) {
		Account account = accountDao.getAccount(id_user);
		Profile profile = accountDao.getProfile(id_user);
		List<Post> posts = postBo.getAllPost(id_user);
		for (Post post : posts) {
			post.getAccount().setProfile(accountDao.getProfile(post.getAccount().getId_user()));
		}
		account.setProfile(profile);
		account.setPosts(posts);
		request.setAttribute("anotherAccount", account);
//		try {
//			request.getRequestDispatcher("WEB-INF/jsp/profile2.jsp").include(request, response);
//		} catch (ServletException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return "profile2";
    }
	
	
}
