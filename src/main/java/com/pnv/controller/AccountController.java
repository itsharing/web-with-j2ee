package com.pnv.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.pnv.business.AccountBo;
import com.pnv.model.Account;


@Controller
public class AccountController {
	@Autowired
    private AccountBo accountBo;

	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(HttpServletRequest request, @RequestParam("email") String email,@RequestParam("password") String password) {
		
			Account account = accountBo.login(email,password);
			if(account != null){
				request.getSession().setAttribute("account", account);
				if(account.getIsActive() == 0){
					return "verify_code";
				}else{
					return "index";
				}
			}else{
				return "register_new";
			}
		
		
    }
	
	@RequestMapping(value = "/logOut", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "register_new";
    }
}
