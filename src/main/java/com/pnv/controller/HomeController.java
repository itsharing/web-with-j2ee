package com.pnv.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.AccountBo;

@Controller
public class HomeController {	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String showHomePage(HttpServletRequest request) {
		if(request.getSession().getAttribute("account") == null){
			return "register_new";
		}else{
			return "home_page";
		}
		
    }
	
	@RequestMapping(value = "/viewMember", method = RequestMethod.GET)
    public String showMemberPage(HttpServletRequest request) {
		return "members";
		
    }
	
	@RequestMapping(value = "/viewPhoto", method = RequestMethod.GET)
    public String showPhotoPage(HttpServletRequest request) {
		return "photos";
		
    }
	
//	@RequestMapping(value = "/viewProfile", method = RequestMethod.GET)
//    public String showProfilePage(HttpServletRequest request) {
//		return "profile2";
//		
//    }
}
